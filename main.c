/*
 * Copyright (c) 2016, Stefan Lankes, RWTH Aachen University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the University nor the names of its contributors
 *      may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <json-c/json.h>
#include <curl/curl.h>

size_t curl_callback (void *contents, size_t size, size_t nmemb, void *userp) {
	size_t realsize = size * nmemb;
	json_object *json;
	enum json_tokener_error jerr = json_tokener_success;

	printf("Receive %zd bytes\n", realsize);

	json = json_tokener_parse_verbose(contents, &jerr);

	if (jerr != json_tokener_success) {
		/* error */
		fprintf(stderr, "ERROR: Failed to parse json string");
		/* free json object */
		json_object_put(json);
		return -1;
	}

	printf("Parsed JSON: %s\n", json_object_to_json_string(json));

	/* free json object */
	json_object_put(json);

	return realsize;
}

int main(int argc, char** argv)
{
	CURL *curl;
	json_object *json;
	CURLcode rcode; /* curl result code */

	curl_global_init(CURL_GLOBAL_ALL);

	/* init curl handle */
	if ((curl = curl_easy_init()) == NULL) {
		/* log error */
		fprintf(stderr, "ERROR: Failed to create curl handle in fetch_session");
		/* return error */
		return 1;
	}

	/* create json object for post */
	json = json_object_new_object();

	/* build post data */
	json_object_object_add(json, "title", json_object_new_string("testies"));
	json_object_object_add(json, "body", json_object_new_string("testies ... testies ... 1,2,3"));
	json_object_object_add(json, "userId", json_object_new_int(133));

	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_setopt(curl, CURLOPT_URL, "http://jsonplaceholder.typicode.com/posts/");
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_object_to_json_string(json));

	/* set calback function */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_callback);

	rcode = curl_easy_perform(curl);

	// cleanup curl 
	curl_easy_cleanup(curl);

	/* free json object */
	json_object_put(json);

	/* check return code */
	if (rcode != CURLE_OK) {
		/* log error */
		fprintf(stderr, "ERROR: Failed to fetch url - curl said: %s", curl_easy_strerror(rcode));
		/* return error */
		return 2;
	}

	return 0;
}
