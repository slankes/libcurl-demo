CC=gcc
CFLAGS=-O2 -Wall

default: curl_demo

curl_demo: main.o
	$(CC) $(CFLAGS) -o $@ $< -lcurl -ljson-c

clean:
	rm -rf *.o
